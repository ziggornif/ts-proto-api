module.exports = {
  root: true,
  parser: '@typescript-eslint/parser',
  parserOptions: {
    project: './tsconfig.eslint.json',
  },
  extends: [
    'airbnb-base',
    'plugin:jsdoc/recommended',
    'airbnb-typescript/base',
    'plugin:prettier/recommended',
  ],
  plugins: ['@typescript-eslint', 'jsdoc'],
  env: {
    node: true,
    jest: true,
    commonjs: true,
    es2021: true,
  },
};

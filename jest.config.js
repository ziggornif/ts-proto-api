module.exports = {
  roots: ['<rootDir>/src'],
  forceExit: true,
  preset: 'ts-jest',
  testEnvironment: 'node',
  reporters: ['default', 'jest-junit'],
  coverageDirectory: '<rootDir>/coverage/',
  testPathIgnorePatterns: ['node_modules'],
  setupFiles: ['dotenv/config'],
};

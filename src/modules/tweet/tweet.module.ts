import { Express } from 'express';
import { PrismaClient } from '@prisma/client';
import TweetController from './tweet.controller';
import TweetRepository from './tweet.repository';
import TweetService from './tweet.service';

class TweetModule {
  #expressApp: Express;

  #tweetController: TweetController;

  #tweetService: TweetService;

  #tweetRepository: TweetRepository;

  constructor(expressApp: Express, prisma: PrismaClient) {
    this.#expressApp = expressApp;
    this.#tweetRepository = new TweetRepository(prisma);
    this.#tweetService = new TweetService(this.#tweetRepository);
    this.#tweetController = new TweetController(this.#tweetService);
  }

  bootstrap() {
    this.#expressApp.use(this.#tweetController.routes());
  }
}

export default TweetModule;

import { PrismaClient, Tweet } from '@prisma/client';

class TweetRepository {
  #prisma: PrismaClient;

  constructor(prisma: PrismaClient) {
    this.#prisma = prisma;
  }

  findMany(): Promise<Tweet[]> {
    return this.#prisma.tweet.findMany();
  }

  async create(
    message: string,
    created_at: Date,
    authorId: string,
  ): Promise<void> {
    await this.#prisma.tweet.create({
      data: {
        message,
        created_at,
        authorId,
      },
    });
  }

  async findById(id: string): Promise<Tweet> {
    const tweet = await this.#prisma.tweet.findFirst({
      where: {
        id,
      },
      rejectOnNotFound: true,
    });
    return tweet;
  }
}

export default TweetRepository;

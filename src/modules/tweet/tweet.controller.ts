import express, { Router, Request, Response } from 'express';
import TweetService from './tweet.service';

class TweetController {
  #router: Router;

  #tweetService: TweetService;

  constructor(tweetService: TweetService) {
    this.#router = express.Router();
    this.#tweetService = tweetService;
  }

  routes(): Router {
    this.#router.get('/tweets', async (req: Request, res: Response) => {
      const tweets = await this.#tweetService.listTweets();
      res.json(tweets);
    });

    this.#router.post('/tweets', async (req: Request, res: Response) => {
      const { message, authorId } = req.body;
      await this.#tweetService.createTweet(message, authorId);
      res.status(201).send();
    });

    this.#router.get('/tweets/:id', async (req: Request, res: Response) => {
      const { id } = req.params;
      const tweet = await this.#tweetService.getTweet(id);
      res.json(tweet);
    });

    return this.#router;
  }
}

export default TweetController;

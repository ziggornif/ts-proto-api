import { Tweet } from '@prisma/client';
import TweetRepository from './tweet.repository';

class TweetService {
  #tweetRepository: TweetRepository;

  constructor(tweetRepository: TweetRepository) {
    this.#tweetRepository = tweetRepository;
  }

  listTweets(): Promise<Tweet[]> {
    return this.#tweetRepository.findMany();
  }

  createTweet(message: string, authorId: string): Promise<void> {
    const date = new Date();
    return this.#tweetRepository.create(message, date, authorId);
  }

  getTweet(id: string): Promise<Tweet> {
    return this.#tweetRepository.findById(id);
  }
}

export default TweetService;

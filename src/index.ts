import express from 'express';
import cors from 'cors';
import { AddressInfo } from 'net';
import { PrismaClient } from '@prisma/client';
import TweetModule from './modules/tweet/tweet.module';
// import { TweetService } from './tweet';

(async function run() {
  const prisma = new PrismaClient();
  const app = express();
  app.use(cors());
  app.use(express.json());
  app.use(
    express.urlencoded({
      extended: false,
    }),
  );

  prisma.$connect();

  const tweetModule = new TweetModule(app, prisma);
  tweetModule.bootstrap();

  const server = app.listen(8080, () => {
    const { port } = server.address() as AddressInfo;
    console.log(`🚀 server started and available on http://localhost:${port}`);
  });
})();
